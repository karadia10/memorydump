package com.example.root.memorydump;

/**
 * Created by root on 23/5/16.
 */
public class SensorEventListener {
    /*What acceleration difference would we assume as a rapid movement? */
    private final float shakeThreshold = 1.5f;
    /* Here we store the current values of acceleration, one for each axis */
    private float xAccel;
    private float yAccel;
    private float zAccel;
    /* And here the previous ones */
    private float xPreviousAccel;
    private float yPreviousAccel;
    private float zPreviousAccel;
    /* Used to suppress the first shaking */
    private boolean firstUpdate = true;
    /* Has a shaking motion been started (one direction) */
    private boolean shakeInitiated = false;
}
