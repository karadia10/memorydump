package com.example.root.memorydump;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 16/5/16.
 */
public class DBHandler extends SQLiteOpenHelper {
    private static int VERSION = 15;
    private static String DB_NAME = "TODO.db";
    //private DBHandler dbHandler;


    public DBHandler(Context context, SQLiteDatabase.CursorFactory factory) {

        super(context, DB_NAME, factory, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE TODO( ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                " TASK TEXT, " +
                " DESCRIPTION TEXT," +
                " DATE TEXT, " +
                " TIME TEXT, " +
                " ALARM TEXT, " +
                " STATUS INTEGER, " +
                " PRIORITY TEXT );");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS TODO;");
        onCreate(sqLiteDatabase);

    }

    public void addTask(TodoList todoList) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("TASK", todoList.getTask());
        contentValues.put("DESCRIPTION", todoList.getDescription());
        contentValues.put("DATE", todoList.getDate());
        contentValues.put("TIME", todoList.getTime());
        contentValues.put("ALARM", todoList.getAlarm());
        contentValues.put("STATUS", todoList.getStatus());
        contentValues.put("PRIORITY", todoList.getPriority());
        this.getWritableDatabase().insertOrThrow("TODO", null, contentValues);

    }

    public List<TodoList> getActiveTask() {
        List<TodoList> todoList = new ArrayList<TodoList>();
        int i = 1;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM TODO WHERE STATUS=1 ORDER BY DATE, TIME ASC;", null);

        cursor.moveToFirst();
        while (cursor.moveToNext()) {
            TodoList todoList1 = new TodoList();
            todoList1.setSno(i);
            todoList1.setTask(cursor.getString(1));
            todoList1.setDescription(cursor.getString(2));
            todoList1.setDate(cursor.getString(3));
            todoList1.setTime(cursor.getString(4));
            todoList1.setAlarm(cursor.getString(5));
            todoList1.setPriority(cursor.getString(7));
            todoList1.setStatus(cursor.getInt(6));
            todoList.add(todoList1);
            i++;

        }
        cursor.close();
        db.close();

        return todoList;
    }

    public List<TodoList> getCompletedTask() {
        List<TodoList> todoList = new ArrayList<TodoList>();
        int i = 1;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM TODO WHERE STATUS=2 ORDER BY DATE, TIME ASC;", null);

        cursor.moveToFirst();
        while (cursor.moveToNext()) {
            TodoList todoList1 = new TodoList();
            todoList1.setSno(i);
            todoList1.setTask(cursor.getString(1));
            todoList1.setDescription(cursor.getString(2));
            todoList1.setDate(cursor.getString(3));
            todoList1.setTime(cursor.getString(4));
            todoList1.setAlarm(cursor.getString(5));
            todoList1.setStatus(cursor.getInt(6));
            todoList1.setPriority(cursor.getString(7));
            todoList.add(todoList1);
            i++;
        }
        cursor.close();
        db.close();

        return todoList;
    }

    public List<TodoList> getArchievedTask() {
        List<TodoList> todoList = new ArrayList<TodoList>();
        int i = 1;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM TODO WHERE STATUS=3 ORDER BY DATE, TIME ASC;", null);

        cursor.moveToFirst();
        while (cursor.moveToNext()) {

            TodoList todoList1 = new TodoList();
            todoList1.setSno(i);
            todoList1.setTask(cursor.getString(1));
            todoList1.setDescription(cursor.getString(2));
            todoList1.setDate(cursor.getString(3));
            todoList1.setTime(cursor.getString(4));
            todoList1.setAlarm(cursor.getString(5));
            todoList1.setStatus(cursor.getInt(6));
            todoList1.setPriority(cursor.getString(7));
            todoList.add(todoList1);
            i++;
        }
        cursor.close();
        db.close();

        return todoList;
    }


    public void deleteTask(TodoList list) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("TODO", "TASK='" + list.getTask() + "' and DESCRIPTION='" + list.getDescription() + "' and TIME='" + list.getTime() + "'", null);

        db.close();
    }


    public void setTaskActive(TodoList todoList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE TODO SET STATUS=1 WHERE TASK='" + todoList.getTask() + "' and DESCRIPTION='" + todoList.getDescription() + "'");

        db.close();
    }


    public void setTaskCompleted(TodoList todoList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE TODO SET STATUS=2 WHERE TASK='" + todoList.getTask() + "' and DESCRIPTION='" + todoList.getDescription() + "'");

        db.close();
    }

    public void setTaskArchieved(TodoList todoList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE TODO SET STATUS=3 WHERE TASK='" + todoList.getTask() + "' and DESCRIPTION='" + todoList.getDescription() + "'");
        db.close();
    }

    public void setTaskDeleted(TodoList todoList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE TODO SET STATUS=4 WHERE TASK='" + todoList.getTask() + "' and DESCRIPTION='" + todoList.getDescription() + "'");
        db.close();
    }

    public void toggleAlarmOff(TodoList todoList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE TODO SET ALARM='OFF' WHERE TASK='" + todoList.getTask() + "' and DESCRIPTION='" + todoList.getDescription() + "'");
        db.close();
    }

    public void toggleAlarmOn(TodoList todoList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE TODO SET ALARM='ON' WHERE TASK='" + todoList.getTask() + "' and DESCRIPTION='" + todoList.getDescription() + "'");
        db.close();
    }


}
