package com.example.root.memorydump;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class CompletedTaskAdapter extends RecyclerView.Adapter<CompletedTaskAdapter.ViewHolder> {

    private List<TodoList> todoList;
    private Context context;


    public CompletedTaskAdapter(List<TodoList> todoList, Context context) {
        this.todoList = todoList;
        this.context = context;
    }

    @Override
    public CompletedTaskAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_completed_task, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CompletedTaskAdapter.ViewHolder holder, final int position) {

        //final int pos=position;
        String s = String.valueOf(position);
        try {

            holder.tv_sno.setText(s);
            holder.tv_task.setText(todoList.get(position).getTask());
            holder.tv_date.setText("Date: " + todoList.get(position).getDate());
            holder.tv_time.setText("Time: " + todoList.get(position).getTime());
            if (todoList.get(position).getPriority().equals("High")) {
                holder.card.setCardBackgroundColor(ContextCompat.getColor(context, R.color.high_priority));
            } else if (todoList.get(position).getPriority().equals("Medium")) {
                holder.card.setCardBackgroundColor(ContextCompat.getColor(context, R.color.medium_priority));

            } else if (todoList.get(position).getPriority().equals("Low")) {
                holder.card.setCardBackgroundColor(Color.WHITE);

            }


        } catch (Exception e) {
        }

    }


    @Override
    public int getItemCount() {
        return todoList.size();
    }

    public List<TodoList> getTodoList() {
        return todoList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_sno, tv_task, tv_date, tv_time;
        //Button b_attend,b_delete;
        public CardView card;

        public ViewHolder(View itemView) {

            super(itemView);
            card = (CardView) itemView.findViewById(R.id.card);
            tv_sno = (TextView) itemView.findViewById(R.id.sno);
            tv_task = (TextView) itemView.findViewById(R.id.task);
            tv_date = (TextView) itemView.findViewById(R.id.date);
            tv_time = (TextView) itemView.findViewById(R.id.time);


        }

    }


}