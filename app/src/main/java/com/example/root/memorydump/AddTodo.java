package com.example.root.memorydump;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class AddTodo extends AppCompatActivity {

    public DBHandler dbHandler;
    public EditText task, description;
    public TextView sel_date, sel_time;
    public Button but_date, but_time;
    public SwitchCompat alarm;
    public Spinner priority;
    public int mYear, mMonth, mDay, mHour, mMinute;
    public String pickDate, pickTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_todo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbHandler = new DBHandler(this, null);
        task = (EditText) findViewById(R.id.task);
        description = (EditText) findViewById(R.id.description);

        alarm = (SwitchCompat) findViewById(R.id.alarm);
        priority = (Spinner) findViewById(R.id.priority);

        sel_date = (TextView) findViewById(R.id.sel_Date);
        sel_time = (TextView) findViewById(R.id.sel_Time);

        but_date = (Button) findViewById(R.id.but_Date);
        but_time = (Button) findViewById(R.id.but_Time);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TodoList todoList1 = new TodoList();
                todoList1.setStatus(1);   //1 for active
                todoList1.setTask(task.getText().toString());
                todoList1.setDescription(description.getText().toString());

                //alarm
                if (alarm.isChecked())
                    todoList1.setAlarm("ON");
                else
                    todoList1.setAlarm("OFF");
                //priority date time
                todoList1.setPriority(String.valueOf(priority.getSelectedItem()));
                todoList1.setTime(String.valueOf(pickTime));
                todoList1.setDate(String.valueOf(pickDate));
                dbHandler.addTask(todoList1);

                Intent i = new Intent(AddTodo.this, MainActivity.class);
                finish();
                startActivity(i);
            }

        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    public void pickDate(View view) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {


                        pickDate = year + "-" + monthOfYear + "-" + dayOfMonth;
                        sel_date.setText(year + "-" + monthOfYear + "-" + dayOfMonth);

                        //Log.d("Date ",pickDate+"\n\n\n\n\n");

                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

    public void pickTime(View view) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        sel_time.setText(hourOfDay + ":" + minute);
                        pickTime = hourOfDay + ":" + minute;
                        //Log.d("Date ",pickTime +"\n\n\n\n\n");
                    }
                }, mHour, mMinute, false);

        timePickerDialog.show();
    }

}

