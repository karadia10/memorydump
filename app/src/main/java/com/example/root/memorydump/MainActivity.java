package com.example.root.memorydump;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final int NOTIFY_ME_ID = 1337;
    private final Context context = this;
    public List<TodoList> todoList;
    public DBHandler dbHandler;
    NotificationManager notificationManager;
    private RecyclerView taskRecyclerView;
    private RecyclerView.Adapter taskRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbHandler = new DBHandler(this, null);

        todoList = new ArrayList<TodoList>();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View view) {
                                       Intent i = new Intent(MainActivity.this, AddTodo.class);
                                       startActivity(i);
                                   }
                               }

        );
        taskRecyclerView = (RecyclerView) findViewById(R.id.todo_recycler_view);
        taskRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }


    public void displayNotification(TodoList todoList) {

        //creating a new intent
        Intent i = new Intent(this, MainActivity.class);
        //i.putExtra("NotificationId",NOTIFY_ME_ID);

        //creating a notification
        notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, 0);
        Notification.Builder builder = new Notification.Builder(this)
                .setContentTitle("Task: " + todoList.getTask())         //must
                .setContentText(todoList.getPriority() + "  " + todoList.getDescription())       //must
                .setSmallIcon(R.drawable.ic_notification)           //must
                .setContentIntent(pendingIntent);

        //display notification
        Notification notify = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            notify = builder.build();
        }
        notificationManager.notify(NOTIFY_ME_ID, notify);
    }

    @Override
    protected void onStart() {


        try {
            todoList = dbHandler.getActiveTask();
        } catch (Exception e) {
            Toast.makeText(context, "List error", Toast.LENGTH_LONG).show();
        }

        taskRecyclerAdapter = new TodoListAdapter(todoList, this);

        taskRecyclerView.setAdapter(taskRecyclerAdapter);


        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                //Remove swiped item from list and notify the RecyclerView
                int pos = viewHolder.getAdapterPosition();
                if (swipeDir == ItemTouchHelper.LEFT) {
                    dbHandler.setTaskArchieved(todoList.get(pos));
                    Toast.makeText(context, "Task: '" + todoList.get(pos).getTask() + "' Archieved", Toast.LENGTH_LONG).show();

                } else if (swipeDir == ItemTouchHelper.RIGHT) {
                    dbHandler.deleteTask(todoList.get(pos));
                    Toast.makeText(context, "Task: '" + todoList.get(pos).getTask() + "' Deleted", Toast.LENGTH_LONG).show();
                }
                onStart();
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(taskRecyclerView);


        taskRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                taskRecyclerView, new ClickListener() {

            @Override
            public void onClick(View view, final int position) {
                //Values are passing to activity & to fragment as well
                Toast.makeText(MainActivity.this, "Description: " + todoList.get(position).getDescription(), Toast.LENGTH_LONG).show();
            }


            @Override
            public void onLongClick(View view, final int position) {

            }

        }));

        super.onStart();
    }


    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            startActivity(new Intent(MainActivity.this, MainActivity.class));
        } else if (id == R.id.nav_completed) {
            startActivity(new Intent(MainActivity.this, CompletedTask.class));
        } else if (id == R.id.nav_archieved) {
            startActivity(new Intent(MainActivity.this, ArchievedTask.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //interface for onsingle tap and longpress
    public static interface ClickListener {


        public void onClick(View view, int position);

        public void onLongClick(View view, int position);

    }

    //class for gestures
    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {


        private ClickListener clicklistener;

        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener) {

            this.clicklistener = clicklistener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recycleView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clicklistener != null) {
                        clicklistener.onLongClick(child, recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }


        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clicklistener != null && gestureDetector.onTouchEvent(e)) {
                clicklistener.onClick(child, rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
