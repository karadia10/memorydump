package com.example.root.memorydump;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ArchievedTask extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private final Context context = this;
    public List<TodoList> todoList;
    public DBHandler dbHandler;
    private RecyclerView taskRecyclerView;
    private RecyclerView.Adapter taskRecyclerAdapter;
    private Button change1, change3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_archieved);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbHandler = new DBHandler(this, null);

        todoList = new ArrayList<TodoList>();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View view) {
                                       Intent i = new Intent(ArchievedTask.this, AddTodo.class);
                                       startActivity(i);
                                   }
                               }

        );
        taskRecyclerView = (RecyclerView) findViewById(R.id.archieved_recycler_view);
        taskRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    protected void onStart() {


        try {
            todoList = dbHandler.getArchievedTask();
        } catch (Exception e) {
            Toast.makeText(context, "List error", Toast.LENGTH_LONG).show();
        }

        taskRecyclerAdapter = new ArchievedTaskAdapter(todoList, this);

        taskRecyclerView.setAdapter(taskRecyclerAdapter);


        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                //Remove swiped item from list and notify the RecyclerView
                int pos = viewHolder.getAdapterPosition();
                if (swipeDir == ItemTouchHelper.LEFT) {
                    dbHandler.setTaskActive(todoList.get(pos));
                    Toast.makeText(context, "Task: '" + todoList.get(pos).getTask() + "' Activated", Toast.LENGTH_LONG).show();

                } else if (swipeDir == ItemTouchHelper.RIGHT) {
                    dbHandler.deleteTask(todoList.get(pos));
                    Toast.makeText(context, "Task: '" + todoList.get(pos).getTask() + "' Deleted", Toast.LENGTH_LONG).show();
                }
                //Toast.makeText(context, "Task Archieved", Toast.LENGTH_LONG).show();

                onStart();

            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(taskRecyclerView);


        taskRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                taskRecyclerView, new ClickListener() {

            @Override
            public void onClick(View view, final int position) {
                //Values are passing to activity & to fragment as well
                Toast.makeText(ArchievedTask.this, "Description: " + todoList.get(position).getDescription(), Toast.LENGTH_LONG).show();
            }


            @Override
            public void onLongClick(View view, final int position) {

            }

        }));

        super.onStart();
    }

    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(this, MainActivity.class));

            // Handle the camera action
        } else if (id == R.id.nav_completed) {
            startActivity(new Intent(this, CompletedTask.class));

        } else if (id == R.id.nav_archieved) {
            startActivity(new Intent(this, ArchievedTask.class));

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void fakedata() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        final String formattedDate = dateFormat.format(c.getTime());
        final String formattedTime = timeFormat.format(c.getTime());
        for (int i = 0; i < 2; i++) {
            TodoList todoList = new TodoList();
            todoList.setTime(String.valueOf(formattedTime));
            todoList.setDate(String.valueOf(formattedDate));
            todoList.setPriority("High");
            todoList.setAlarm("ON");
            todoList.setStatus(1);
            todoList.setDescription("My first alarm");
            todoList.setTask("1st");

            dbHandler.addTask(todoList);

        }
    }

    //interface for onsingle tap and longpress
    public static interface ClickListener {


        public void onClick(View view, int position);

        public void onLongClick(View view, int position);

    }

    //class for gestures
    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {


        private ClickListener clicklistener;

        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener) {

            this.clicklistener = clicklistener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recycleView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clicklistener != null) {
                        clicklistener.onLongClick(child, recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }


        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clicklistener != null && gestureDetector.onTouchEvent(e)) {
                clicklistener.onClick(child, rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
