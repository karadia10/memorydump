package com.example.root.memorydump;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

public class TodoListAdapter extends RecyclerView.Adapter<TodoListAdapter.ViewHolder> {

    private List<TodoList> todoList;
    private Context context;
    private DBHandler dbHandler;


    public TodoListAdapter(List<TodoList> todoList, Context context) {
        this.todoList = todoList;
        this.context = context;
    }

    @Override
    public TodoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_task_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        dbHandler = new DBHandler(context, null);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final TodoListAdapter.ViewHolder holder, final int position) {

        //final int pos=position;
        String s = String.valueOf(position);
        try {

            holder.tv_sno.setText(s);
            holder.tv_task.setText(todoList.get(position).getTask());
            holder.tv_date.setText("Date: " + todoList.get(position).getDate());
            holder.tv_time.setText("Time: " + todoList.get(position).getTime());


            if (todoList.get(position).getPriority().equals("High")) {
                holder.card.setCardBackgroundColor(ContextCompat.getColor(context, R.color.high_priority));
            } else if (todoList.get(position).getPriority().equals("Medium")) {
                holder.card.setCardBackgroundColor(ContextCompat.getColor(context, R.color.medium_priority));

            } else if (todoList.get(position).getPriority().equals("Low")) {
                holder.card.setCardBackgroundColor(Color.WHITE);

            }
            if (todoList.get(position).getAlarm().equals("ON")) {
                holder.alarm.setChecked(true);

            }
            holder.alarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (todoList.get(position).getAlarm().equals("ON")) {
                        dbHandler.toggleAlarmOff(todoList.get(position));
                    } else if (todoList.get(position).getAlarm().equals("OFF")) {
                        dbHandler.toggleAlarmOn(todoList.get(position));
                    }
                    Snackbar.make(holder.itemView, "Switch " + position + "\n", Snackbar.LENGTH_LONG).show();
                }
            });


        } catch (Exception e) {
        }

    }


    @Override
    public int getItemCount() {
        return todoList.size();
    }

    public List<TodoList> getTodoList() {
        return todoList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_sno, tv_task, tv_date, tv_time;
        //Button b_attend,b_delete;
        public SwitchCompat alarm;
        public CardView card;

        public ViewHolder(View itemView) {

            super(itemView);
            card = (CardView) itemView.findViewById(R.id.card);
            tv_sno = (TextView) itemView.findViewById(R.id.sno);
            tv_task = (TextView) itemView.findViewById(R.id.task);
            tv_date = (TextView) itemView.findViewById(R.id.date);
            tv_time = (TextView) itemView.findViewById(R.id.time);
            alarm = (SwitchCompat) itemView.findViewById(R.id.alarm);

        }

    }


}